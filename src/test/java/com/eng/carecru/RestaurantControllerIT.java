package com.eng.carecru;

import com.eng.carecru.presentation.rest.dtos.DailyWorkingRangeDTO;
import com.eng.carecru.presentation.rest.dtos.RestaurantDTO;
import com.eng.carecru.presentation.rest.dtos.RestaurantScheduleDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestaurantApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class RestaurantControllerIT extends BaseControllerIT {
    @LocalServerPort
    int port;

    @Test
    public void addRestaurant() {
        HttpEntity<RestaurantDTO> entity = new HttpEntity<>(RestaurantDTO.builder().name("Alfred Jr").build(),headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort(port,"/restaurants"),
                HttpMethod.POST, entity, String.class);

        String actual = Objects.requireNonNull(response.getHeaders().get(HttpHeaders.LOCATION)).get(0);

        assertTrue(actual.contains("/restaurants/1"));
    }

    @Test
    public void addRestaurantAlreadyAdded() {
        HttpEntity<RestaurantDTO> entity = new HttpEntity<>(RestaurantDTO
                .builder()
                .id(1L)
                .name("Alfred Jr")
                .build(), headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort(port,"/restaurants"),
                HttpMethod.POST, entity, String.class);

        assertSame(response.getStatusCode(), HttpStatus.CONFLICT);
    }

    @Test
    public void addRestaurantSchedule() {
        List<DailyWorkingRangeDTO> dailyWorkingRanges = new ArrayList<>();
        dailyWorkingRanges.add(DailyWorkingRangeDTO.builder()
                .openTime(LocalTime.of(10,0))
                .closeTime(LocalTime.of(14,0))
                .build());
        dailyWorkingRanges.add(DailyWorkingRangeDTO.builder()
                .openTime(LocalTime.of(17,0))
                .closeTime(LocalTime.of(23,59))
                .build());

        RestaurantScheduleDTO scheduleDTO = new RestaurantScheduleDTO();
        scheduleDTO.setOpenDate(LocalDate.now().plusDays(2));
        scheduleDTO.setCloseDate(LocalDate.now().plusDays(5));
        scheduleDTO.setDailyWorkingRanges(dailyWorkingRanges);

        HttpEntity<RestaurantScheduleDTO> entity = new HttpEntity<>(scheduleDTO);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort(port,"/restaurants/1/schedule"),
                HttpMethod.POST, entity, String.class);
        Assert.assertSame(HttpStatus.CREATED, response.getStatusCode());
        showResult();
    }

    private void showResult() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort(port, "restaurants/1"));

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<String> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        System.out.println(response.getBody());
    }
}
