package com.eng.carecru;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Suite.class)
@SpringBootTest
@Suite.SuiteClasses({
    JPAEntitiesTest.class, ServicesTest.class, RestaurantControllerIT.class, ReservationServiceTest.class
})
public class RestaurantApplicationTests {

  @Test
  public void contextLoads() {
  }
}
