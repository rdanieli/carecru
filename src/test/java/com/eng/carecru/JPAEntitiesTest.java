package com.eng.carecru;

import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantAvailabilityDailyRange;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class JPAEntitiesTest {
  @PersistenceContext
  EntityManager entityManager;

  @Test
  @Transactional
  public void testInsertNewUser() {
    User user = new User();
    user.setName("Alfred");
    user.setCtrDthInc(LocalDateTime.now());
    entityManager.persist(user);
    entityManager.flush();
    assertNotNull(user.getId());
  }

  @Test
  @Transactional
  public void testInsertNewRestaurant() {
    Restaurant restaurant = new Restaurant();
    restaurant.setName("Starbucks");
    restaurant.setCtrDthInc(LocalDateTime.now());
    entityManager.persist(restaurant);
    entityManager.flush();
    assertNotNull(restaurant.getId());
  }

  @Test
  @Transactional
  public void testInsertNewRestaurantAllDependents() {
    RestaurantSchedule restaurantSchedule = createRestaurantAndAllDependencies();
    assertNotNull(restaurantSchedule.getId());
  }

  private RestaurantSchedule createRestaurantAndAllDependencies() {
    Restaurant restaurant = new Restaurant();
    restaurant.setName("The Scheduled Restaurant");
    restaurant.setCtrDthInc(LocalDateTime.now());

    RestaurantSchedule restaurantSchedule = new RestaurantSchedule();
    List<RestaurantAvailabilityDailyRange> operatingRangeList = new ArrayList<>();

    operatingRangeList.add(RestaurantAvailabilityDailyRange.builder()
            .openTime(LocalTime.of(11, 0))
            .closeTime(LocalTime.of(15, 0))
            .ctrDthInc(LocalDateTime.now())
            .build());

    operatingRangeList.add(RestaurantAvailabilityDailyRange.builder()
            .openTime(LocalTime.of(19, 0))
            .closeTime(LocalTime.of(23, 59))
            .ctrDthInc(LocalDateTime.now())
            .build());

    restaurantSchedule.setOperatingRange(operatingRangeList);

    restaurantSchedule.setOpenDate(LocalDate.now());
    restaurantSchedule.setCloseDate(LocalDate.now().plusDays(3));
    restaurantSchedule.setCtrDthInc(LocalDateTime.now());
    restaurantSchedule.setRestaurant(restaurant);
    entityManager.persist(restaurantSchedule);
    entityManager.flush();
    return restaurantSchedule;
  }


}
