package com.eng.carecru;

import com.eng.carecru.application.services.RestaurantService;
import com.eng.carecru.application.services.UserService;
import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantAvailabilityDailyRange;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class ServicesTest {

  @Autowired
  private UserService userService;

  @Autowired
  private RestaurantService restaurantService;

  @Test
  public void testInsertUser() {
    User user = new User();
    user.setName("Alfred Jr.");
    user.setCtrDthInc(LocalDateTime.now());

    userService.addUser(user);

    assertNotNull(user.getId());
  }

  @Test
  @Transactional
  public void testInsertRestaurantSchedule() {
    Restaurant restaurant = new Restaurant();
    restaurant.setName("Ground Fire Barbecue");

    restaurantService.addRestaurant(restaurant);

    RestaurantSchedule restaurantSchedule = new RestaurantSchedule();
    restaurantSchedule.setOpenDate(LocalDate.now().plusDays(1));
    restaurantSchedule.setCloseDate(LocalDate.now().plusDays(8));
    List<RestaurantAvailabilityDailyRange> dailyWorkingRanges = new ArrayList<>();
    dailyWorkingRanges.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(10,0))
        .closeTime(LocalTime.of(14,0))
        .build());
    dailyWorkingRanges.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(17,0))
        .closeTime(LocalTime.of(23,59))
        .build());
    restaurantSchedule.setOperatingRange(dailyWorkingRanges);
    restaurantService.addRestaurantSchedule(restaurant, restaurantSchedule);

    Optional<Restaurant> restaurantById = restaurantService.findRestaurantById(restaurant.getId());

    assertFalse(restaurantById.get().getRestaurantSchedules().isEmpty());
  }

  @Test
  @Transactional
  public void testInsertRestaurantScheduleConsideringNotEmpty() {
    Restaurant restaurant = new Restaurant();
    restaurant.setName("Ground Fire Barbecue");

    restaurantService.addRestaurant(restaurant);

    List<RestaurantAvailabilityDailyRange> dailyWorkingRanges = new ArrayList<>();
    dailyWorkingRanges.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(10, 0))
        .closeTime(LocalTime.of(14, 0))
        .build());
    dailyWorkingRanges.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(17, 0))
        .closeTime(LocalTime.of(23, 59))
        .build());
    RestaurantSchedule restaurantSchedule = new RestaurantSchedule();
    restaurantSchedule.setOpenDate(LocalDate.now().plusDays(8));
    restaurantSchedule.setCloseDate(LocalDate.now().plusDays(10));
    restaurantSchedule.setOperatingRange(dailyWorkingRanges);

    restaurantService.addRestaurantSchedule(restaurant, restaurantSchedule);

    List<RestaurantAvailabilityDailyRange> dailyWorkingRanges1 = new ArrayList<>();
    dailyWorkingRanges1.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(10, 0))
        .closeTime(LocalTime.of(14, 0))
        .build());
    dailyWorkingRanges1.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(17, 0))
        .closeTime(LocalTime.of(23, 59))
        .build());

    RestaurantSchedule restaurantSchedule1 = new RestaurantSchedule();
    restaurantSchedule1.setOpenDate(LocalDate.now().plusDays(11));
    restaurantSchedule1.setCloseDate(LocalDate.now().plusDays(12));
    restaurantSchedule1.setOperatingRange(dailyWorkingRanges1);

    restaurantService.addRestaurantSchedule(restaurant, restaurantSchedule1);

    Optional<Restaurant> restaurantById = restaurantService.findRestaurantById(restaurant.getId());

    assertEquals(2, restaurantById.get().getRestaurantSchedules().size());
  }
}
