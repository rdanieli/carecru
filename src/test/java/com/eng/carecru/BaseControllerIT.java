package com.eng.carecru;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;

class BaseControllerIT {
    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    String createURLWithPort(int port, String uri) {
        return "http://localhost:" + port + uri;
    }
}
