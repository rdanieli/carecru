package com.eng.carecru;

import com.eng.carecru.application.services.ReservationPaymentService;
import com.eng.carecru.application.services.ReservationService;
import com.eng.carecru.application.services.RestaurantService;
import com.eng.carecru.application.services.UserService;
import com.eng.carecru.application.services.exceptions.BusinessException;
import com.eng.carecru.domain.Reservation;
import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantAvailabilityDailyRange;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class ReservationServiceTest {

  @Autowired
  private UserService userService;

  @Autowired
  private RestaurantService restaurantService;

  @Autowired
  private ReservationService reservationService;

  @Autowired
  private ReservationPaymentService reservationPaymentService;

  private User user;
  private Restaurant restaurant;
  private RestaurantSchedule restaurantSchedule;

  @Before
  public void prepare() {
    user = prepareUser();
    restaurant = prepareRestaurant();
    restaurantSchedule = prepareRestaurantSchedule(restaurant);
  }

  @Test
  @Transactional
  public void listAllAvailableSlots() {
    List allAvailableSlots = reservationService.getAllAvailableSlots(LocalDateTime.now().minusDays(1), LocalDateTime.now().plusDays(2), restaurant.getId());
    assertFalse(allAvailableSlots.isEmpty());
  }

  @Test
  @Transactional
  public void testBookingReservation() {
    Reservation reservation = Reservation.builder()
                                          .user(user)
                                          .restaurantSchedule(restaurantSchedule)
                                          .seats(2)
                                          .build();
    LocalDateTime localDateTime = LocalDateTime.now().plusDays(2).withHour(11).withMinute(30).withSecond(0).withNano(0);
    reservationService.bookReservation(reservation, localDateTime);
  }

  @Test
  @Transactional
  public void testCancelReservation() {
    Reservation reservation = Reservation.builder()
            .user(user)
            .restaurantSchedule(restaurantSchedule)
            .seats(2)
            .build();
    LocalDateTime localDateTime = LocalDateTime.now().plusDays(1).withHour(11).withMinute(30).withSecond(0).withNano(0);
    reservation = reservationService.bookReservation(reservation, localDateTime);

    Optional<Reservation> reservationEntity = reservationService.findReservationEntity(reservation.getId());
    reservationService.cancelReservation(reservationEntity.get().getId(), reservationEntity.get().getUser().getId());
//    Assert.assertEquals(ReservationStatus.NEW, reservationEntity.get().);
  }

  @Test
  @Transactional
  public void testRefundUserWhole(){
    assertEquals(0, reservationPaymentService.getDiscountValue(10.0, LocalDateTime.now().until(LocalDateTime.now(), ChronoUnit.MINUTES)), 0);
    assertEquals(7.5, reservationPaymentService.getDiscountValue(10.0, LocalDateTime.now().minusMinutes(10).until(LocalDateTime.now(), ChronoUnit.MINUTES)), 0);
    assertEquals(5, reservationPaymentService.getDiscountValue(10.0, LocalDateTime.now().minusHours(11).until(LocalDateTime.now(), ChronoUnit.MINUTES)), 0);
    assertEquals(2.5, reservationPaymentService.getDiscountValue(10.0, LocalDateTime.now().minusHours(13).until(LocalDateTime.now(), ChronoUnit.MINUTES)), 0);
    assertEquals(10, reservationPaymentService.getDiscountValue(10.0, LocalDateTime.now().minusHours(25).until(LocalDateTime.now(), ChronoUnit.MINUTES)), 0);
  }

  @Test(expected = BusinessException.class)
  @Transactional
  public void testBookingReservationWithoutSchedulePlanned() {
    Reservation reservation = Reservation.builder()
                                          .user(user)
                                          .restaurantSchedule(restaurantSchedule)
                                          .seats(2)
                                          .build();
    LocalDateTime localDateTime = LocalDateTime.now();//.plusDays(2).withHour(11).withMinute(30);
    reservationService.bookReservation(reservation, localDateTime);
  }

  private RestaurantSchedule prepareRestaurantSchedule(Restaurant restaurant) {
    RestaurantSchedule restaurantSchedule = new RestaurantSchedule();
    restaurantSchedule.setOpenDate(LocalDate.now().plusDays(1));
    restaurantSchedule.setCloseDate(LocalDate.now().plusDays(8));
    List<RestaurantAvailabilityDailyRange> dailyWorkingRanges = new ArrayList<>();
    dailyWorkingRanges.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(10,0))
        .closeTime(LocalTime.of(14,0))
        .build());
    dailyWorkingRanges.add(RestaurantAvailabilityDailyRange.builder()
        .openTime(LocalTime.of(17,0))
        .closeTime(LocalTime.of(23,59))
        .build());
    restaurantSchedule.setOperatingRange(dailyWorkingRanges);
    restaurantService.addRestaurantSchedule(restaurant, restaurantSchedule);
    return restaurantSchedule;
  }

  private Restaurant prepareRestaurant() {
    Restaurant restaurant = new Restaurant();
    restaurant.setName("Barbecue Gaucho");

    restaurantService.addRestaurant(restaurant);
    return restaurant;
  }

  private User prepareUser() {
    User user = new User();
    user.setName("Alfred Jr.");
    user.setCtrDthInc(LocalDateTime.now());

    userService.addUser(user);
    return user;
  }
}
