package com.eng.carecru.domain;

/**
 * The enum Reservation status.
 */
public enum ReservationStatus {
  /**
   * New reservation status.
   */
  NEW, //Created new reservation
  /**
   * Payment verified reservation status.
   */
  PAYMENT_VERIFIED, //verified payment and accepted by the restaurant
  /**
   * The Done.
   */
  DONE //finished the reservation and applied rules
}
