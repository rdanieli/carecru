package com.eng.carecru.domain;

import com.eng.carecru.infrastructure.persistence.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

/**
 * The type Restaurant schedule.
 */
@Entity
@Table(name = "restaurant_schedule")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
@Builder
public class RestaurantSchedule extends BaseEntity<Long> {

  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "restaurant_id", nullable = false)
  private Restaurant restaurant;

  @Column
  @NotNull(message = "Open Date cannot be null")
  private LocalDate openDate;

  @Column
  @NotNull(message = "Close Date cannot be null")
  private LocalDate closeDate;

  @Enumerated
  @NotNull
  private RestaurantScheduleStatus scheduleStatus = RestaurantScheduleStatus.OPEN;

  @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
  @JoinColumn(name = "operating_range_id", nullable = false)
  private List<RestaurantAvailabilityDailyRange> operatingRange;

}
