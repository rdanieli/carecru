package com.eng.carecru.domain;

/**
 * The enum Payment rules.
 */
public enum PaymentRules {
  /**
   * The Keep none.
   */
  KEEP_NONE {
        @Override
        public Double calculate(Double value) {
            return value;
        }
    },
  /**
   * The Keep 25 percent.
   */
  KEEP_25_PERCENT {
        @Override
        public Double calculate(Double value) {
            return (value * 25) / 100;
        }
    },
  /**
   * The Keep 50 percent.
   */
  KEEP_50_PERCENT {
        @Override
        public Double calculate(Double value) {
            return (value * 50) / 100;
        }
    }, /**
   * The Keep 75 percent.
   */
  KEEP_75_PERCENT {
        @Override
        public Double calculate(Double value) {
            return (value * 75) / 100;
        }
    }, /**
   * The Keep 100 percent.
   */
  KEEP_100_PERCENT {
        @Override
        public Double calculate(Double value) {
            return 0.0;
        }
    };


  /**
   * Calculate double.
   *
   * @param value the value
   * @return the double
   */
  public abstract Double calculate(Double value);
}
