package com.eng.carecru.domain;

import com.eng.carecru.infrastructure.persistence.BaseEntity;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * The type Restaurant availability daily range.
 */
@Entity
@Table(name = "restaurant_availability_daily_range")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class RestaurantAvailabilityDailyRange extends BaseEntity<Long> {

  @Column
  @NotNull
  private LocalTime openTime;

  @Column
  @NotNull
  private LocalTime closeTime;

  /**
   * Instantiates a new Restaurant availability daily range.
   *
   * @param id          the id
   * @param ctrNroIpAtu the ctr nro ip atu
   * @param ctrUsuInc   the ctr usu inc
   * @param ctrDthAtu   the ctr dth atu
   * @param ctrNroIpInc the ctr nro ip inc
   * @param ctrDthInc   the ctr dth inc
   * @param openTime    the open time
   * @param closeTime   the close time
   */
  @Builder
  public RestaurantAvailabilityDailyRange(Long id, String ctrNroIpAtu, Long ctrUsuInc, LocalDateTime ctrDthAtu, String ctrNroIpInc, @NotNull LocalDateTime ctrDthInc, @NotNull LocalTime openTime, @NotNull LocalTime closeTime) {
    super(id, ctrNroIpAtu, ctrUsuInc, ctrDthAtu, ctrNroIpInc, ctrDthInc);
    this.openTime = openTime;
    this.closeTime = closeTime;
  }
}
