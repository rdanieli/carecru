package com.eng.carecru.domain;

/**
 * The enum Restaurant schedule status.
 */
public enum RestaurantScheduleStatus {
  /**
   * Open restaurant schedule status.
   */
  OPEN,
  /**
   * Closed restaurant schedule status.
   */
  CLOSED
}
