package com.eng.carecru.domain;

import com.eng.carecru.infrastructure.persistence.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The type Reservation.
 */
@Entity
@Table(name = "reservations")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
@Builder
public class Reservation extends BaseEntity<Long> {

    @ManyToOne
    private User user;

    @ManyToOne
    @NotNull
    private RestaurantSchedule restaurantSchedule;

    @Column
    private Integer seats;

    @Column
    private LocalDateTime reservationDateTime;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy = "reservation")
    private List<Payment> reservationPayments;
}
