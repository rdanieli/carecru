package com.eng.carecru.presentation.rest.rest;

import com.eng.carecru.infrastructure.persistence.BaseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * The type Base rest controller.
 */
public class BaseRestController {
  /**
   * Location by entity uri.
   *
   * @param entity the entity
   * @return the uri
   */
  protected URI locationByEntity(BaseEntity entity){
        return ServletUriComponentsBuilder.fromCurrentRequest().path(
                "/{id}").buildAndExpand(entity.getId()).toUri();
    }
}
