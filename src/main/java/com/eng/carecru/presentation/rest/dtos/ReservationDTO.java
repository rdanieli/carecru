package com.eng.carecru.presentation.rest.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * The type Reservation dto.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Data
public class ReservationDTO {
  private Long id;

  @NotNull
  private Long userId;

  @NotNull
  private Long restaurantId;

  @NotNull
  private LocalDateTime reservationDateTime;

  @NotNull
  @Max(10)
  @Min(1)
  private Integer seats;
}
