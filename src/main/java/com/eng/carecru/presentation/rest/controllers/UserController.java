package com.eng.carecru.presentation.rest.controllers;

import com.eng.carecru.application.services.UserService;
import com.eng.carecru.domain.User;
import com.eng.carecru.presentation.rest.dtos.UserDTO;
import com.eng.carecru.presentation.rest.rest.BaseRestController;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * The type User controller.
 */
@RestController("users")
@RequestMapping("/users")
public class UserController extends BaseRestController {

  private final UserService userService;

  private final ModelMapper modelMapper;

  /**
   * Instantiates a new User controller.
   *
   * @param userService the user service
   * @param modelMapper the model mapper
   */
  @Autowired
  public UserController(UserService userService, ModelMapper modelMapper) {
    this.userService = userService;
    this.modelMapper = modelMapper;
  }

  /**
   * New user response entity.
   *
   * @param userDTO the user dto
   * @return the response entity
   */
  @PostMapping
  public ResponseEntity<Void> newUser(@RequestBody UserDTO userDTO) {
    User user = userService.addUser(modelMapper.map(userDTO, User.class));

    if (user == null) {
      return ResponseEntity.noContent().build();
    }

    return ResponseEntity.created(locationByEntity(user)).build();
  }

  /**
   * Find user info response entity.
   *
   * @param userId the user id
   * @return the response entity
   */
  @GetMapping("/{userId}")
  public ResponseEntity<UserDTO> findUserInfo(@PathVariable Long userId) {
    Optional<User> optUser = userService.findUserById(userId);
    return optUser.map(user -> ResponseEntity.ok(modelMapper.map(user, UserDTO.class))).orElseGet(() -> ResponseEntity.noContent().build());
  }
}
