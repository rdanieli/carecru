package com.eng.carecru.presentation.rest.controllers;

import com.eng.carecru.application.services.ReservationService;
import com.eng.carecru.domain.Reservation;
import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.domain.User;
import com.eng.carecru.presentation.rest.dtos.ReservationDTO;
import com.eng.carecru.presentation.rest.rest.BaseRestController;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Reservation controller.
 */
@RestController(value = "reservations")
@RequestMapping("/reservations")
public class ReservationController extends BaseRestController {

  private final ReservationService reservationService;

  private final ModelMapper modelMapper;

  /**
   * Instantiates a new Reservation controller.
   *
   * @param reservationService the reservation service
   * @param modelMapper        the model mapper
   */
  @Autowired
  public ReservationController(ReservationService reservationService, ModelMapper modelMapper) {
    this.reservationService = reservationService;
    this.modelMapper = modelMapper;
  }

  /**
   * Book reservation response entity.
   *
   * @param reservationDTO the reservation dto
   * @return the response entity
   */
  @PostMapping
  public ResponseEntity<Void> bookReservation(@Valid @RequestBody ReservationDTO reservationDTO) {
    Reservation reservation = reservationService.bookReservation(convertToEntity(reservationDTO), reservationDTO.getReservationDateTime());

    if (reservation == null) {
      return ResponseEntity.noContent().build();
    }

    return ResponseEntity.created(locationByEntity(reservation)).build();
  }

  /**
   * Find reservation info response entity.
   *
   * @param reservationId the reservation id
   * @return the response entity
   */
  @GetMapping("/{reservationId}")
  public ResponseEntity<ReservationDTO> findReservationInfo(@PathVariable Long reservationId) {
    Optional<Reservation> optReservation = reservationService.findReservationEntity(reservationId);

    if(optReservation.isPresent() && optReservation.get().getUser() == null){
      return ResponseEntity.noContent().build();
    }

    return optReservation.map(restaurant -> ResponseEntity.ok(entityToDto(restaurant))).orElseGet(() -> ResponseEntity.noContent().build());
  }

  /**
   * Cancel reservation response entity.
   *
   * @param reservationId the reservation id
   * @param userId        the user id
   * @return the response entity
   */
  @DeleteMapping("/{reservationId}/users/{userId}")
  public ResponseEntity<Void> cancelReservation(@PathVariable Long reservationId, @PathVariable Long userId) {
    reservationService.cancelReservation(reservationId, userId);
    return ResponseEntity.noContent().build();
  }

  /**
   * Reschedule reservation response entity.
   *
   * @param oldReservationId the old reservation id
   * @param userId           the user id
   * @param reservationDTO   the reservation dto
   * @return the response entity
   */
  @PutMapping("/reschedule/{oldReservationId}/users/{userId}")
  public ResponseEntity<?> rescheduleReservation(@PathVariable Long oldReservationId, @PathVariable Long userId, @Valid @RequestBody ReservationDTO reservationDTO) {
    Reservation reservation = reservationService.rescheduleReservation(oldReservationId, userId, convertToEntity(reservationDTO));

    if (reservation == null) {
      return ResponseEntity.noContent().build();
    }

    return ResponseEntity.ok(locationByEntity(reservation));
  }

  /**
   * Past reservations response entity.
   *
   * @param restaurantId the restaurant id
   * @return the response entity
   */
  @GetMapping("/restaurant/{restaurantId}")
  public ResponseEntity<List<ReservationDTO>> pastReservations(@PathVariable Long restaurantId) {
    List<Reservation> freeSlots = reservationService.getAllPastReservations(restaurantId);
    if(freeSlots == null || freeSlots.isEmpty()){
      return ResponseEntity.noContent().build();
    }
    List<ReservationDTO> newValues = freeSlots.stream().map(this::entityToDto).collect(Collectors.toList());
    return ResponseEntity.ok(newValues);
  }

  /**
   * List available slots response entity.
   *
   * @param startDate    the start date
   * @param endDate      the end date
   * @param restaurantId the restaurant id
   * @return the response entity
   */
  @GetMapping("/search")
  public ResponseEntity<List<ReservationDTO>> listAvailableSlots(@RequestParam("startDate")
                                                                 @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                                 @RequestParam("endDate")
                                                                 @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                                                 @RequestParam(value = "restaurantId", required = false) Long restaurantId) {
    List<Reservation> freeSlots = reservationService.getAllAvailableSlots(LocalDateTime.of(startDate, LocalTime.of(0, 0)), LocalDateTime.of(endDate, LocalTime.of(23,59)), restaurantId);
    if(freeSlots == null || freeSlots.isEmpty()){
      return ResponseEntity.noContent().build();
    }
    List<ReservationDTO> newValues = freeSlots.stream().map(this::entityToDto).collect(Collectors.toList());
    return ResponseEntity.ok(newValues);
  }

  private ReservationDTO entityToDto(Reservation reservation) {
    ReservationDTO reservationDTO = modelMapper.map(reservation, ReservationDTO.class);

    if(reservation.getUser() != null) {
      reservationDTO.setUserId(reservation.getUser().getId());
    }

    reservationDTO.setRestaurantId(reservation.getRestaurantSchedule().getRestaurant().getId());
    return reservationDTO;
  }

  private Reservation convertToEntity(ReservationDTO restaurantDTO) {
    Reservation reservation = modelMapper.map(restaurantDTO, Reservation.class);

    User user = new User();
    user.setId(restaurantDTO.getUserId());
    reservation.setUser(user);

    RestaurantSchedule restaurantSchedule = new RestaurantSchedule();
    Restaurant restaurant = new Restaurant();
    restaurant.setId(restaurantDTO.getRestaurantId());
    restaurantSchedule.setRestaurant(restaurant);

    reservation.setRestaurantSchedule(restaurantSchedule);

    return reservation;
  }
}
