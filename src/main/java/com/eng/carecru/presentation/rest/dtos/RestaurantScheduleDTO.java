package com.eng.carecru.presentation.rest.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

/**
 * The type Restaurant schedule dto.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Data
public class RestaurantScheduleDTO {

    private RestaurantDTO restaurant;

    private Long id;
    private LocalDate openDate;
    private LocalDate closeDate;

    private List<DailyWorkingRangeDTO> dailyWorkingRanges;
}
