package com.eng.carecru.presentation.rest.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;

/**
 * The type Daily working range dto.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DailyWorkingRangeDTO {

    private Long id;

    private LocalTime openTime;

    private LocalTime closeTime;
}
