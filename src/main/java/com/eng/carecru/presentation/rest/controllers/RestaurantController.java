package com.eng.carecru.presentation.rest.controllers;

import com.eng.carecru.application.services.RestaurantService;
import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantAvailabilityDailyRange;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.presentation.rest.dtos.RestaurantDTO;
import com.eng.carecru.presentation.rest.dtos.RestaurantScheduleDTO;
import com.eng.carecru.presentation.rest.rest.BaseRestController;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The type Restaurant controller.
 */
@RestController("restaurants")
@RequestMapping("/restaurants")
public class RestaurantController extends BaseRestController {

  private final RestaurantService restaurantService;

  private final ModelMapper modelMapper;

  /**
   * Instantiates a new Restaurant controller.
   *
   * @param restaurantService the restaurant service
   * @param modelMapper       the model mapper
   */
  @Autowired
  public RestaurantController(RestaurantService restaurantService, ModelMapper modelMapper) {
    this.restaurantService = restaurantService;
    this.modelMapper = modelMapper;
  }

  /**
   * New restaurant response entity.
   *
   * @param restaurantDTO the restaurant dto
   * @return the response entity
   */
  @PostMapping
  public ResponseEntity<Void> newRestaurant(@RequestBody RestaurantDTO restaurantDTO) {
    Restaurant restaurant = restaurantService.addRestaurant(modelMapper.map(restaurantDTO, Restaurant.class));

    if (restaurant == null) {
      return ResponseEntity.noContent().build();
    }

    return ResponseEntity.created(locationByEntity(restaurant)).build();
  }

  /**
   * New restaurant schedule response entity.
   *
   * @param restaurantId          the restaurant id
   * @param restaurantScheduleDTO the restaurant schedule dto
   * @return the response entity
   */
  @PostMapping(value = "/{restaurantId}/schedule")
  public ResponseEntity<Void> newRestaurantSchedule(@PathVariable Long restaurantId, @RequestBody RestaurantScheduleDTO restaurantScheduleDTO) {
    Restaurant restaurant = new Restaurant();
    restaurant.setId(restaurantId);

    Restaurant result = restaurantService.addRestaurantSchedule(restaurant, convertToEntity(restaurantScheduleDTO));

    if (result == null) {
      return ResponseEntity.noContent().build();
    }

    return ResponseEntity.created(locationByEntity(result)).build();
  }

  private RestaurantSchedule convertToEntity(RestaurantScheduleDTO restaurantScheduleDTO) {
    RestaurantSchedule schedule = modelMapper.map(restaurantScheduleDTO, RestaurantSchedule.class);

    if (restaurantScheduleDTO.getDailyWorkingRanges() != null) {
      schedule.setOperatingRange(restaurantScheduleDTO.getDailyWorkingRanges().stream().map(dailyWorkingRange -> RestaurantAvailabilityDailyRange.builder()
          .id(dailyWorkingRange.getId())
          .openTime(dailyWorkingRange.getOpenTime())
          .closeTime(dailyWorkingRange.getCloseTime()).build()).collect(Collectors.toList()));
    }
    return schedule;
  }

  /**
   * Find restaurant info response entity.
   *
   * @param restaurantId the restaurant id
   * @return the response entity
   */
  @GetMapping("/{restaurantId}")
  public ResponseEntity<RestaurantDTO> findRestaurantInfo(@PathVariable Long restaurantId) {
    Optional<Restaurant> optRestaurant = restaurantService.findRestaurantById(restaurantId);
    return optRestaurant.map(restaurant -> ResponseEntity.ok(modelMapper.map(restaurant, RestaurantDTO.class))).orElseGet(() -> ResponseEntity.noContent().build());
  }

}
