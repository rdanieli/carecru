package com.eng.carecru;

import com.eng.carecru.infrastructure.audit.ReservationAware;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * The type Restaurant application.
 */
@SpringBootApplication
@EnableJpaAuditing
public class RestaurantApplication {

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(RestaurantApplication.class, args);
  }

  /**
   * Model mapper model mapper.
   *
   * @return the model mapper
   */
  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }

  /**
   * Auditor provider reservation aware.
   *
   * @return the reservation aware
   */
  @Bean
  public ReservationAware auditorProvider() {
    return new ReservationAware();
  }
}