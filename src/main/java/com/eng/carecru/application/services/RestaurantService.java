package com.eng.carecru.application.services;

import com.eng.carecru.application.services.exceptions.AlreadyExistsEntityException;
import com.eng.carecru.application.services.exceptions.BusinessException;
import com.eng.carecru.application.services.exceptions.EntityNotFoundException;
import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.infrastructure.repositories.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

/**
 * The type Restaurant service.
 */
@Service
public class RestaurantService extends BaseService {

  private final RestaurantRepository restaurantRepository;

  private final ReservationService reservationService;

  /**
   * Instantiates a new Restaurant service.
   *
   * @param restaurantRepository the restaurant repository
   * @param reservationService   the reservation service
   */
  @Autowired
  public RestaurantService(RestaurantRepository restaurantRepository, ReservationService reservationService) {
    this.restaurantRepository = restaurantRepository;
    this.reservationService = reservationService;
  }

  /**
   * Add restaurant restaurant.
   *
   * @param restaurant the restaurant
   * @return the restaurant
   */
//Assumption: A restaurant COULD have the same name than other included due to we don't mind about address.
  public Restaurant addRestaurant(final Restaurant restaurant) {
    if (restaurant.getId() != null) {
      Optional<Restaurant> restaurantOptional = restaurantRepository.findById(restaurant.getId());
      if (restaurantOptional.isPresent()) {
        throw new AlreadyExistsEntityException("This restaurant you are attempting add is already included.");
      } else {
        return restaurantRepository.saveAndFlush(restaurant);
      }
    } else {
      return restaurantRepository.saveAndFlush(restaurant);
    }
  }

  /**
   * Add restaurant schedule restaurant.
   *
   * @param restaurantInput    the restaurant input
   * @param restaurantSchedule the restaurant schedule
   * @return the restaurant
   */
  public Restaurant addRestaurantSchedule(final Restaurant restaurantInput, final @Valid RestaurantSchedule restaurantSchedule) {
    verifyAllInputIsValid(restaurantSchedule);
    restaurantSchedule.getOperatingRange().forEach(this::verifyAllInputIsValid);

    if (restaurantInput.getId() == null) {
      throw new IllegalArgumentException("Restaurant id cannot be null.");
    }

    Optional<Restaurant> optionalRestaurant = restaurantRepository.findById(restaurantInput.getId());

    if (!optionalRestaurant.isPresent()) {
      throw new EntityNotFoundException(String.format("Restaurant #%s not found.", restaurantInput.getId()));
    }

    verifyScheduleDates(restaurantSchedule);

    Restaurant restaurant = optionalRestaurant.get();
    restaurantSchedule.setRestaurant(restaurant);

    Restaurant savedRestaurant = addRestaurantSchedule(restaurantSchedule, restaurant);

    reservationService.createReservationsBySchedule(restaurantSchedule);

    return savedRestaurant;
  }

  private Restaurant addRestaurantSchedule(RestaurantSchedule restaurantSchedule, Restaurant restaurant) {
    if (restaurant.getRestaurantSchedules() != null &&
        !restaurant.getRestaurantSchedules().isEmpty()) {
      restaurant.getRestaurantSchedules().forEach(rs -> {
        boolean before = rs.getCloseDate().isBefore(restaurantSchedule.getOpenDate());

        if (!before) {
          throw new BusinessException("The start date informed overlaps a period that already exists.");
        }
      });

      restaurant.getRestaurantSchedules().add(restaurantSchedule);
    } else {
      ArrayList<RestaurantSchedule> schedules = new ArrayList<>();
      schedules.add(restaurantSchedule);
      restaurant.setRestaurantSchedules(schedules);
    }
    return restaurantRepository.saveAndFlush(restaurant);
  }

  private void verifyScheduleDates(RestaurantSchedule restaurantSchedule) {
    if (restaurantSchedule.getOpenDate().isBefore(LocalDate.now())) {
      throw new BusinessException("You inform a date before the actual day.");
    }

    if (restaurantSchedule.getOpenDate().isAfter(restaurantSchedule.getCloseDate())) {
      throw new BusinessException("End date cannot be lower than Start date.");
    }

    if(restaurantSchedule.getOperatingRange() != null){
      restaurantSchedule.getOperatingRange().forEach(dailyRange -> {
        boolean isAfter = dailyRange.getOpenTime().isAfter(dailyRange.getCloseTime());
        if(isAfter){
          throw new BusinessException("Open hour cannot be greater than Closed hour.");
        }
      });
    }
  }

  /**
   * Find restaurant by id optional.
   *
   * @param id the id
   * @return the optional
   */
  public Optional<Restaurant> findRestaurantById(Long id) {
    return restaurantRepository.findById(id);
  }
}
