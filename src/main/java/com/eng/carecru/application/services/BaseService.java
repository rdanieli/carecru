package com.eng.carecru.application.services;

import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

/**
 * The type Base service.
 */
@Transactional
public abstract class BaseService {

    @Autowired
    private Validator validator;

  /**
   * Verify all input is valid.
   *
   * @param <T>   the type parameter
   * @param input the input
   */
  <T> void verifyAllInputIsValid(T input){
        Set<ConstraintViolation<T>> violations = validator.validate(input);
        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<?> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }
            throw new ConstraintViolationException("Error occurred: " + sb.toString(), violations);
        }
    }

}
