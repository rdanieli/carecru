package com.eng.carecru.application.services;

import com.eng.carecru.domain.PaymentRules;
import com.eng.carecru.domain.Reservation;
import com.eng.carecru.domain.Payment;
import com.eng.carecru.domain.ReservationStatus;
import com.eng.carecru.infrastructure.repositories.ReservationPaymentRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * The type Reservation payment service.
 */
@Service
public class ReservationPaymentService extends BaseService {

  private ReservationPaymentRepository reservationPaymentRepository;

  /**
   * Instantiates a new Reservation payment service.
   *
   * @param reservationPaymentRepository the reservation payment repository
   */
  public ReservationPaymentService(ReservationPaymentRepository reservationPaymentRepository) {
    this.reservationPaymentRepository = reservationPaymentRepository;
  }

  /**
   * Add reservation deposit.
   *
   * @param reservation the reservation
   */
  public void addReservationDeposit(Reservation reservation) {
    Payment reservationPayment = Payment.builder()
        .reservation(reservation)
        .reservationStatus(ReservationStatus.PAYMENT_VERIFIED)
        .value(Double.valueOf(reservation.getSeats() * 10))
        .build();

    reservationPaymentRepository.saveAndFlush(reservationPayment);
  }

  /**
   * Refund user.
   *
   * @param reservation the reservation
   */
  public void refundUser(Reservation reservation) {
    LocalDateTime now = LocalDateTime.now();

    long remainingMinutes = now.until(reservation.getReservationDateTime(), ChronoUnit.MINUTES);
    Payment reservationPayment = reservationPaymentRepository.findByReservationAndReservationStatus(reservation, ReservationStatus.PAYMENT_VERIFIED);

    reservationPayment.setValue(reservationPayment.getValue() - getDiscountValue(reservationPayment.getValue(), remainingMinutes));

    reservationPayment.setReservationStatus(ReservationStatus.DONE);
    reservationPaymentRepository.saveAndFlush(reservationPayment);
  }

  /**
   * Gets discount value.
   *
   * @param value            the value
   * @param remainingMinutes the remaining minutes
   * @return the discount value
   */
  public Double getDiscountValue(Double value, long remainingMinutes) {
    PaymentRules paymentRule;

    if(refundWholeValue(remainingMinutes)){
      paymentRule = PaymentRules.KEEP_NONE;
    } else if (keep25Value(remainingMinutes)) {
      paymentRule = PaymentRules.KEEP_25_PERCENT;
    } else if (keep50Value(remainingMinutes)){
      paymentRule = PaymentRules.KEEP_50_PERCENT;
    } else if (keep75Value(remainingMinutes)){
      paymentRule = PaymentRules.KEEP_75_PERCENT;
    } else {
      paymentRule = PaymentRules.KEEP_100_PERCENT;
    }

    return paymentRule.calculate(value);
  }

  private boolean keep75Value(long remainingMinutes) {
    return remainingMinutes <= (2 * 60) && remainingMinutes > 0;
  }

  private boolean keep50Value(long remainingMinutes) {
    return remainingMinutes < (12 * 60) && remainingMinutes >= (2 * 60);
  }

  private boolean keep25Value(long remainingMinutes) {
    return remainingMinutes < (24 * 60) && remainingMinutes >= (12 * 60);
  }

  private boolean refundWholeValue(long remainingMinutes) {
    return remainingMinutes >= (24 * 60);
  }
}
