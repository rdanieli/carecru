package com.eng.carecru.application.services;

import com.eng.carecru.domain.User;
import com.eng.carecru.infrastructure.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Optional;

/**
 * The type User service.
 */
@Service
public class UserService extends BaseService {

    private final UserRepository userRepository;

  /**
   * Instantiates a new User service.
   *
   * @param userRepository the user repository
   */
  @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

  /**
   * Add user user.
   *
   * @param user the user
   * @return the user
   */
  public User addUser(@Valid User user) {
      verifyAllInputIsValid(user);

      return userRepository.saveAndFlush(user);
    }

  /**
   * Find user by id optional.
   *
   * @param userId the user id
   * @return the optional
   */
  public Optional<User> findUserById(Long userId) {
    return userRepository.findById(userId);
  }
}
