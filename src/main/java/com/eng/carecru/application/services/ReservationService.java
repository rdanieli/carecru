package com.eng.carecru.application.services;

import com.eng.carecru.application.services.exceptions.BusinessException;
import com.eng.carecru.domain.Reservation;
import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantAvailabilityDailyRange;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.domain.RestaurantScheduleStatus;
import com.eng.carecru.domain.User;
import com.eng.carecru.infrastructure.repositories.ReservationRepository;
import com.eng.carecru.infrastructure.repositories.RestaurantScheduleRepository;
import com.eng.carecru.infrastructure.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * The type Reservation service.
 */
@Service
public class ReservationService extends BaseService {

  private final ReservationRepository reservationRepository;

  private final UserRepository userRepository;

  private final RestaurantScheduleRepository restaurantScheduleRepository;

  private final ReservationPaymentService reservationPaymentService;

  /**
   * Instantiates a new Reservation service.
   *
   * @param restaurantRepository         the restaurant repository
   * @param userRepository               the user repository
   * @param restaurantScheduleRepository the restaurant schedule repository
   * @param reservationPaymentService    the reservation payment service
   */
  @Autowired
  public ReservationService(ReservationRepository restaurantRepository,
                            UserRepository userRepository,
                            RestaurantScheduleRepository restaurantScheduleRepository,
                            ReservationPaymentService reservationPaymentService) {
    this.reservationRepository = restaurantRepository;
    this.userRepository = userRepository;
    this.restaurantScheduleRepository = restaurantScheduleRepository;
    this.reservationPaymentService = reservationPaymentService;
  }

  /**
   * Create reservations by schedule.
   *
   * @param rs the rs
   */
  public void createReservationsBySchedule(final RestaurantSchedule rs) {
    Optional<RestaurantSchedule> restaurantScheduleopt = restaurantScheduleRepository.findRestaurantScheduleByOpenDateAndCloseDate(rs.getOpenDate(), rs.getCloseDate());

    RestaurantSchedule restaurantSchedule = restaurantScheduleopt.orElseThrow(() -> new BusinessException("There's not schedule planned for this restaurant"));

    LocalDate date = restaurantSchedule.getOpenDate();

    while (date.isBefore(restaurantSchedule.getCloseDate()) || date.equals(restaurantSchedule.getCloseDate())) {
      for (RestaurantAvailabilityDailyRange dailyRange : restaurantSchedule.getOperatingRange()) {
        LocalDateTime startPeriod = LocalDateTime.of(date, dailyRange.getOpenTime());
        LocalDateTime endPeriod = LocalDateTime.of(date, dailyRange.getCloseTime());

        for (; startPeriod.isBefore(endPeriod); startPeriod = startPeriod.plusHours(1).plusMinutes(30)) {
          Reservation reservation = Reservation.builder()
              .restaurantSchedule(restaurantSchedule)
              .reservationDateTime(startPeriod)
              .build();
          reservationRepository.saveAndFlush(reservation);
        }
      }

      date = date.plusDays(1);
    }
  }

  /**
   * Book reservation reservation.
   *
   * @param reservation the reservation
   * @param givenTime   the given time
   * @return the reservation
   */
  public Reservation bookReservation(Reservation reservation, LocalDateTime givenTime) {
    Optional<RestaurantSchedule> optionalRestaurantSchedule = restaurantScheduleRepository.findByRestaurantAndScheduleStatusAndOpenDateIsLessThanEqualAndCloseDateIsGreaterThanEqual(reservation.getRestaurantSchedule().getRestaurant(), RestaurantScheduleStatus.OPEN, givenTime.toLocalDate(), givenTime.toLocalDate());

    RestaurantSchedule restaurantSchedule = optionalRestaurantSchedule.orElseThrow(() -> new BusinessException("No restaurant schedule found for the given date time."));

    Optional<Reservation> optionalReservation = reservationRepository.findByRestaurantScheduleAndReservationDateTime(restaurantSchedule, givenTime);

    Reservation savedReservation = optionalReservation.orElseThrow(() -> new BusinessException("Any reservation found for the given time."));

    if(savedReservation.getUser() != null) {
      throw new BusinessException("This reservation is already in use.");
    } else {
      Optional<User> optionalUser = userRepository.findById(reservation.getUser().getId());

      savedReservation.setUser(optionalUser.orElseThrow(() -> new BusinessException("User not found.")));
      savedReservation.setSeats(reservation.getSeats());
      reservationRepository.saveAndFlush(savedReservation);
      reservationPaymentService.addReservationDeposit(savedReservation);
    }
    return savedReservation;
  }

  /**
   * Find reservation entity optional.
   *
   * @param reservationId the reservation id
   * @return the optional
   */
  public Optional<Reservation> findReservationEntity(Long reservationId) {
    return reservationRepository.findById(reservationId);
  }

  /**
   * Gets all available slots.
   *
   * @param startDate    the start date
   * @param endDate      the end date
   * @param restaurantId the restaurant id
   * @return the all available slots
   */
  public List<Reservation> getAllAvailableSlots(LocalDateTime startDate, LocalDateTime endDate, Long restaurantId) {
    if(restaurantId == null) {
      return reservationRepository.findAllByReservationDateTimeIsBetweenAndUserIsNull(startDate,
          endDate);
    } else {
      Restaurant restaurant = new Restaurant();
      restaurant.setId(restaurantId);
      Optional<RestaurantSchedule> restaurantSchedule = restaurantScheduleRepository.findByRestaurant(restaurant);
      return restaurantSchedule.map(restaurantSchedule1 -> reservationRepository.findAllByReservationDateTimeIsBetweenAndRestaurantScheduleAndUserIsNull(startDate,
          endDate,
          restaurantSchedule1)).orElse(null);
    }
  }

  /**
   * Cancel reservation.
   *
   * @param reservationId the reservation id
   * @param userId        the user id
   */
  public void cancelReservation(Long reservationId, Long userId) {
    Optional<Reservation> optionalReservation = reservationRepository.findById(reservationId);
    Reservation reservation = optionalReservation.orElseThrow(() -> new BusinessException("Entity not found for delete."));

    if(reservation.getUser() == null || !userId.equals(reservation.getUser().getId())){
      throw new BusinessException("This reservation do not belong to this user");
    }

    reservationPaymentService.refundUser(reservation);

    reservation.setSeats(null);
    reservation.setUser(null);
    reservationRepository.saveAndFlush(reservation);
  }

  /**
   * Reschedule reservation reservation.
   *
   * @param oldReservationId the old reservation id
   * @param userId           the user id
   * @param entity           the entity
   * @return the reservation
   */
  public Reservation rescheduleReservation(Long oldReservationId, Long userId, Reservation entity) {
    cancelReservation(oldReservationId, userId);
    return bookReservation(entity, entity.getReservationDateTime());
  }

  /**
   * Gets all past reservations.
   *
   * @param restaurantId the restaurant id
   * @return all past reservations
   */
  public List<Reservation> getAllPastReservations(Long restaurantId) {
    Restaurant restaurant = new Restaurant();
    restaurant.setId(restaurantId);
    Optional<RestaurantSchedule> restaurantSchedule = restaurantScheduleRepository.findByRestaurant(restaurant);
    return restaurantSchedule.map(restaurantSchedule1 -> reservationRepository.findAllByReservationDateTimeIsBeforeAndRestaurantScheduleAndUserIsNotNull(LocalDateTime.now(), restaurantSchedule.get())).orElse(null);
  }
}
