package com.eng.carecru.infrastructure.repositories;

import com.eng.carecru.domain.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Restaurant repository.
 */
public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
}
