package com.eng.carecru.infrastructure.repositories;

import com.eng.carecru.domain.Reservation;
import com.eng.carecru.domain.RestaurantSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * The interface Reservation repository.
 */
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
  /**
   * Find by restaurant schedule and reservation date time optional.
   *
   * @param restaurantSchedule the restaurant schedule
   * @param dateTime           the date time
   * @return the optional
   */
  Optional<Reservation> findByRestaurantScheduleAndReservationDateTime(RestaurantSchedule restaurantSchedule, LocalDateTime dateTime);

  /**
   * Find all by reservation date time is between and user is null list.
   *
   * @param startDate the start date
   * @param endDate   the end date
   * @return the list
   */
  List<Reservation> findAllByReservationDateTimeIsBetweenAndUserIsNull(LocalDateTime startDate, LocalDateTime endDate);

  /**
   * Find all by reservation date time is between and restaurant schedule and user is null list.
   *
   * @param startDate          the start date
   * @param endDate            the end date
   * @param restaurantSchedule the restaurant schedule
   * @return the list
   */
  List<Reservation> findAllByReservationDateTimeIsBetweenAndRestaurantScheduleAndUserIsNull(LocalDateTime startDate, LocalDateTime endDate, RestaurantSchedule restaurantSchedule);

  /**
   * Find all by reservation date time is before and restaurant schedule and user is not null list.
   *
   * @param date               the date
   * @param restaurantSchedule the restaurant schedule
   * @return the list
   */
  List<Reservation> findAllByReservationDateTimeIsBeforeAndRestaurantScheduleAndUserIsNotNull(LocalDateTime date, RestaurantSchedule restaurantSchedule);
}
