package com.eng.carecru.infrastructure.repositories;

import com.eng.carecru.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface User repository.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
