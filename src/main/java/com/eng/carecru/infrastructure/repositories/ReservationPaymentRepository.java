package com.eng.carecru.infrastructure.repositories;

import com.eng.carecru.domain.Reservation;
import com.eng.carecru.domain.Payment;
import com.eng.carecru.domain.ReservationStatus;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Reservation payment repository.
 */
public interface ReservationPaymentRepository extends JpaRepository<Payment, Long> {
  /**
   * Find by reservation and reservation status payment.
   *
   * @param reservation       the reservation
   * @param reservationStatus the reservation status
   * @return the payment
   */
  Payment findByReservationAndReservationStatus(Reservation reservation, ReservationStatus reservationStatus);
}
