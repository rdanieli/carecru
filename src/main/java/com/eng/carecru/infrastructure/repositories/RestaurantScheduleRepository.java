package com.eng.carecru.infrastructure.repositories;

import com.eng.carecru.domain.Restaurant;
import com.eng.carecru.domain.RestaurantSchedule;
import com.eng.carecru.domain.RestaurantScheduleStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * The interface Restaurant schedule repository.
 */
public interface RestaurantScheduleRepository extends JpaRepository<RestaurantSchedule, Long> {

  /**
   * Find restaurant schedule by open date and close date optional.
   *
   * @param openDate  the open date
   * @param closeDate the close date
   * @return the optional
   */
  Optional<RestaurantSchedule> findRestaurantScheduleByOpenDateAndCloseDate(LocalDate openDate, LocalDate closeDate);

  /**
   * Find by restaurant and schedule status and open date is less than equal and close date is greater than equal optional.
   *
   * @param restaurant     the restaurant
   * @param scheduleStatus the schedule status
   * @param startDate      the start date
   * @param endDate        the end date
   * @return the optional
   */
  Optional<RestaurantSchedule> findByRestaurantAndScheduleStatusAndOpenDateIsLessThanEqualAndCloseDateIsGreaterThanEqual(Restaurant restaurant, RestaurantScheduleStatus scheduleStatus, LocalDate startDate, LocalDate endDate);

  /**
   * Find all by open date is greater than equal and close date is less than equal optional.
   *
   * @param startDate the start date
   * @param endDate   the end date
   * @return the optional
   */
  Optional<List<RestaurantSchedule>> findAllByOpenDateIsGreaterThanEqualAndCloseDateIsLessThanEqual(LocalDate startDate, LocalDate endDate);

  /**
   * Find by restaurant optional.
   *
   * @param restaurant the restaurant
   * @return the optional
   */
  Optional<RestaurantSchedule> findByRestaurant(Restaurant restaurant);
}
