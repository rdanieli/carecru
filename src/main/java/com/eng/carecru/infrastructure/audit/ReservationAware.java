package com.eng.carecru.infrastructure.audit;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * The type Reservation aware.
 */
public class ReservationAware implements AuditorAware {
    @Override
    public Optional getCurrentAuditor() {
        return Optional.empty();
    }
}
