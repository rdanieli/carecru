package com.eng.carecru.infrastructure.persistence;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

/**
 * The type Base entity before persist behavior.
 */
public class BaseEntityBeforePersistBehavior {
  /**
   * Before save.
   *
   * @param entity the entity
   */
  @PrePersist
    public void beforeSave(final BaseEntity entity) {
        if(entity.getCtrDthInc() == null) {
            entity.setCtrDthInc(LocalDateTime.now());
        }
    }

  /**
   * Before update.
   *
   * @param entity the entity
   */
  @PreUpdate
    public void beforeUpdate(final BaseEntity entity) {
        if(entity.getCtrDthInc() == null) {
            entity.setCtrDthInc(LocalDateTime.now());
        }

        if(entity.getCtrDthAtu() == null){
            entity.setCtrDthAtu(LocalDateTime.now());
        }
    }
}
