package com.eng.carecru.infrastructure.persistence;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The type Base entity.
 *
 * @param <ID> the type parameter
 */
@MappedSuperclass
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners({AuditingEntityListener.class})
public abstract class BaseEntity<ID> implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private ID id;

  @Column
  private String ctrNroIpAtu;

  @Column
  private Long ctrUsuInc;

  @Column
  @LastModifiedDate
  private LocalDateTime ctrDthAtu;

  @Column
  private String ctrNroIpInc;

  @Column
  @CreatedDate
  private LocalDateTime ctrDthInc;
}
